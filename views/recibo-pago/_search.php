<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReciboPagoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recibo-pago-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_cat_estatus') ?>

    <?= $form->field($model, 'id_cat_forma_pago') ?>

    <?= $form->field($model, 'tabulador_general_id') ?>

    <?= $form->field($model, 'id_persona') ?>

    <?php // echo $form->field($model, 'numero_recibo_comprobante') ?>

    <?php // echo $form->field($model, 'fecha_pago') ?>

    <?php // echo $form->field($model, 'monto_pagado') ?>

    <?php // echo $form->field($model, 'descripcion_pago') ?>

    <?php // echo $form->field($model, 'bactivo') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'fecha_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
