<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReciboPago */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recibo Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-pago-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_cat_estatus',
            'id_cat_forma_pago',
            'tabulador_general_id',
            'id_persona',
            'numero_recibo_comprobante',
            'fecha_pago',
            'monto_pagado',
            'descripcion_pago',
            'bactivo',
            'fecha_creacion',
            'fecha_update',
        ],
    ]) ?>

</div>
