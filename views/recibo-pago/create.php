<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReciboPago */

$this->title = 'Create Recibo Pago';
$this->params['breadcrumbs'][] = ['label' => 'Recibo Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-pago-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
