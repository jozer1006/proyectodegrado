<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReciboPagoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recibo Pagos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-pago-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Recibo Pago', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_cat_estatus',
            'id_cat_forma_pago',
            'tabulador_general_id',
            'id_persona',
            //'numero_recibo_comprobante',
            //'fecha_pago',
            //'monto_pagado',
            //'descripcion_pago',
            //'bactivo',
            //'fecha_creacion',
            //'fecha_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
