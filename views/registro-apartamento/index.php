<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegistroApartamentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registro Apartamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registro-apartamento-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Registro Apartamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_cat_estatus',
            'id_persona',
            'numero_apto',
            'num_piso',
            //'bactivo',
            //'fecha_creacion',
            //'fecha_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
