<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegistroApartamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registro-apartamento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_cat_estatus')->textInput() ?>

    <?= $form->field($model, 'id_persona')->textInput() ?>

    <?= $form->field($model, 'numero_apto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_piso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bactivo')->textInput() ?>

    <?= $form->field($model, 'fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'fecha_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
