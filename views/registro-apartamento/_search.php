<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegistroApartamentoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registro-apartamento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_cat_estatus') ?>

    <?= $form->field($model, 'id_persona') ?>

    <?= $form->field($model, 'numero_apto') ?>

    <?= $form->field($model, 'num_piso') ?>

    <?php // echo $form->field($model, 'bactivo') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'fecha_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
