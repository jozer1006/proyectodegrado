<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persona-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Persona', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_cat_estatus',
            'primer_nombre_persona',
            'segundo_nombre_persona',
            'primer_apellido_persona',
            //'segundo_apellido_persona',
            //'ci_persona',
            //'dueno',
            //'bactivo',
            //'fecha_creacion',
            //'fecha_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
