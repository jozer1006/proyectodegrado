<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persona-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_cat_estatus') ?>

    <?= $form->field($model, 'primer_nombre_persona') ?>

    <?= $form->field($model, 'segundo_nombre_persona') ?>

    <?= $form->field($model, 'primer_apellido_persona') ?>

    <?php // echo $form->field($model, 'segundo_apellido_persona') ?>

    <?php // echo $form->field($model, 'ci_persona') ?>

    <?php // echo $form->field($model, 'dueno') ?>

    <?php // echo $form->field($model, 'bactivo') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'fecha_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
