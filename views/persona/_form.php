<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Persona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persona-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_cat_estatus')->textInput() ?>

    <?= $form->field($model, 'primer_nombre_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segundo_nombre_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primer_apellido_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segundo_apellido_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ci_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dueno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bactivo')->textInput() ?>

    <?= $form->field($model, 'fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'fecha_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
