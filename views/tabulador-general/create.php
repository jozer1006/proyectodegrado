<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TabuladorGeneral */

$this->title = 'Create Tabulador General';
$this->params['breadcrumbs'][] = ['label' => 'Tabulador Generals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tabulador-general-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
