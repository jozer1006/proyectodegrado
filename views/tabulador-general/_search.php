<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TabuladorGeneralSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tabulador-general-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'monto_a_pagar') ?>

    <?= $form->field($model, 'fecha_tope_a_pagar') ?>

    <?= $form->field($model, 'mes_a_pagar') ?>

    <?= $form->field($model, 'observaciones') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'fecha_update') ?>

    <?php // echo $form->field($model, 'cat_estatus_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
