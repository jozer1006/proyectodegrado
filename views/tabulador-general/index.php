<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TabuladorGeneralSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tabulador Generals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tabulador-general-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tabulador General', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'monto_a_pagar',
            'fecha_tope_a_pagar',
            'mes_a_pagar',
            'observaciones',
            //'fecha_creacion',
            //'fecha_update',
            //'cat_estatus_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
