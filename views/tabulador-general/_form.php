<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TabuladorGeneral */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tabulador-general-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'monto_a_pagar')->textInput() ?>

    <?= $form->field($model, 'fecha_tope_a_pagar')->textInput() ?>

    <?= $form->field($model, 'mes_a_pagar')->textInput() ?>

    <?= $form->field($model, 'observaciones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'fecha_update')->textInput() ?>

    <?= $form->field($model, 'cat_estatus_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
