<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


use webvimark\modules\UserManagement\components\GhostMenu;

use webvimark\modules\UserManagement\components\GhostNav;

use webvimark\modules\UserManagement\UserManagementModule;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html xml:lang="es" lang="es">
<!--<html lang="<?= Yii::$app->language ?>">-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    /*NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top my-navbar',
        ],
    ]);*/

    NavBar::begin([
        'brandLabel' => 'SIGEREPA',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top my-navbar',
        ],

    ]);


echo GhostNav::widget([

        'options'=>['class'=>'navbar-nav navbar-right','id'=>'w1'],
        'encodeLabels'=>false,
        'activateParents'=>false,
        'items' => [
            [
                'label' => 'Backend routes',
                'items'=>UserManagementModule::menuItems()

            ],

            [
                'label' => 'Frontend routes',
                'items'=>[
                    ['label'=>'Login', 'url'=>['/user-management/auth/login']],
                    ['label'=>'Logout', 'url'=>['/user-management/auth/logout']],
                    ['label'=>'Registration', 'url'=>['/user-management/auth/registration']],
                    ['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
                    ['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
                    ['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],

            ['label' => 'Inicio', 'url' => ['/site/index']],
            ['label' => 'Personas', 'url' => ['/persona/index'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Registro Apartamento', 'url' => ['/registro-apartamento/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            
            ['label' => 'Tabulador General', 'url' => ['/tabulador-general/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            ['label' => 'Recibo de Pago', 'url' => ['/recibo-pago/index'], 'visible' => !Yii::$app->user->isGuest]
            ,            

                ],

            ],

        ],

    ]);

NavBar::end();



/*
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Inicio', 'url' => ['/site/index']],
            ['label' => 'Personas', 'url' => ['/persona/index'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Registro Apartamento', 'url' => ['/registro-apartamento/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            ['label' => 'Ususarios', 'url' => ['/usuarios/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            ['label' => 'Tabulador Condominio', 'url' => ['/tabulador-condominio/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            /*['label' => 'Roles', 'url' => ['/cat-roles/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            ['label' => 'Monto Pago', 'url' => ['/cat-monto-pago/index'], 'visible' => !Yii::$app->user->isGuest]

            ,
            ['label' => 'Forma Pago', 'url' => ['/cat-forma-pago/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            ['label' => 'Pregunta Seguridad', 'url' => ['/cat-preguntas-seg/index'], 'visible' => !Yii::$app->user->isGuest]
            ,
            ['label' => 'Estatus', 'url' => ['/cat-estatus/index'], 'visible' => !Yii::$app->user->isGuest]
            , *                     
            Yii::$app->user->isGuest ? (
                ['label' => 'Ingresar', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Salir (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();*/
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Sigerepa <?= date('Y') ?></p>
        <!--<p class="pull-left">&copy; Sigerepa <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
