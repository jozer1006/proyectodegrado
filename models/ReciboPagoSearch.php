<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReciboPago;

/**
 * ReciboPagoSearch represents the model behind the search form of `\app\models\ReciboPago`.
 */
class ReciboPagoSearch extends ReciboPago
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_cat_estatus', 'id_cat_forma_pago', 'tabulador_general_id', 'id_persona', 'bactivo'], 'integer'],
            [['numero_recibo_comprobante', 'fecha_pago', 'descripcion_pago', 'fecha_creacion', 'fecha_update'], 'safe'],
            [['monto_pagado'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReciboPago::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cat_estatus' => $this->id_cat_estatus,
            'id_cat_forma_pago' => $this->id_cat_forma_pago,
            'tabulador_general_id' => $this->tabulador_general_id,
            'id_persona' => $this->id_persona,
            'fecha_pago' => $this->fecha_pago,
            'monto_pagado' => $this->monto_pagado,
            'bactivo' => $this->bactivo,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_update' => $this->fecha_update,
        ]);

        $query->andFilterWhere(['like', 'numero_recibo_comprobante', $this->numero_recibo_comprobante])
            ->andFilterWhere(['like', 'descripcion_pago', $this->descripcion_pago]);

        return $dataProvider;
    }
}
