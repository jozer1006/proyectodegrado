<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TabuladorGeneral;

/**
 * TabuladorGeneralSearch represents the model behind the search form of `\app\models\TabuladorGeneral`.
 */
class TabuladorGeneralSearch extends TabuladorGeneral
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cat_estatus_id'], 'integer'],
            [['monto_a_pagar'], 'number'],
            [['fecha_tope_a_pagar', 'mes_a_pagar', 'observaciones', 'fecha_creacion', 'fecha_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TabuladorGeneral::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'monto_a_pagar' => $this->monto_a_pagar,
            'fecha_tope_a_pagar' => $this->fecha_tope_a_pagar,
            'mes_a_pagar' => $this->mes_a_pagar,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_update' => $this->fecha_update,
            'cat_estatus_id' => $this->cat_estatus_id,
        ]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
