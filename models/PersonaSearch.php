<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Persona;

/**
 * PersonaSearch represents the model behind the search form of `\app\models\Persona`.
 */
class PersonaSearch extends Persona
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_cat_estatus', 'bactivo'], 'integer'],
            [['primer_nombre_persona', 'segundo_nombre_persona', 'primer_apellido_persona', 'segundo_apellido_persona', 'ci_persona', 'dueno', 'fecha_creacion', 'fecha_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Persona::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cat_estatus' => $this->id_cat_estatus,
            'bactivo' => $this->bactivo,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_update' => $this->fecha_update,
        ]);

        $query->andFilterWhere(['like', 'primer_nombre_persona', $this->primer_nombre_persona])
            ->andFilterWhere(['like', 'segundo_nombre_persona', $this->segundo_nombre_persona])
            ->andFilterWhere(['like', 'primer_apellido_persona', $this->primer_apellido_persona])
            ->andFilterWhere(['like', 'segundo_apellido_persona', $this->segundo_apellido_persona])
            ->andFilterWhere(['like', 'ci_persona', $this->ci_persona])
            ->andFilterWhere(['like', 'dueno', $this->dueno]);

        return $dataProvider;
    }
}
