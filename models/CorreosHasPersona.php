<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "correos_has_persona".
 *
 * @property int $correos_id
 * @property int $persona_id
 *
 * @property Correos $correos
 * @property Persona $persona
 */
class CorreosHasPersona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'correos_has_persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['correos_id', 'persona_id'], 'required'],
            [['correos_id', 'persona_id'], 'integer'],
            [['correos_id', 'persona_id'], 'unique', 'targetAttribute' => ['correos_id', 'persona_id']],
            [['correos_id'], 'exist', 'skipOnError' => true, 'targetClass' => Correos::className(), 'targetAttribute' => ['correos_id' => 'id']],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'correos_id' => 'Correos ID',
            'persona_id' => 'Persona ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreos()
    {
        return $this->hasOne(Correos::className(), ['id' => 'correos_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }
}
