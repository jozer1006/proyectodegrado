<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string $numero
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property string $fecha_update
 * @property int $cat_estatus_id
 *
 * @property CatEstatus $catEstatus
 * @property TelefonosHasPersona[] $telefonosHasPersonas
 * @property Persona[] $personas
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'numero', 'descripcion', 'cat_estatus_id'], 'required'],
            [['id', 'cat_estatus_id'], 'integer'],
            [['fecha_creacion', 'fecha_update'], 'safe'],
            [['numero'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 225],
            [['id'], 'unique'],
            [['cat_estatus_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['cat_estatus_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'descripcion' => 'Descripcion',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
            'cat_estatus_id' => 'Cat Estatus ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatEstatus()
    {
        return $this->hasOne(CatEstatus::className(), ['id' => 'cat_estatus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonosHasPersonas()
    {
        return $this->hasMany(TelefonosHasPersona::className(), ['telefonos_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['id' => 'persona_id'])->viaTable('telefonos_has_persona', ['telefonos_id' => 'id']);
    }
}
