<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property int $id
 * @property int $id_cat_estatus
 * @property string $primer_nombre_persona
 * @property string $segundo_nombre_persona
 * @property string $primer_apellido_persona
 * @property string $segundo_apellido_persona
 * @property string $ci_persona
 * @property string $dueno
 * @property int $bactivo
 * @property string $fecha_creacion
 * @property string $fecha_update
 *
 * @property CorreosHasPersona[] $correosHasPersonas
 * @property Correos[] $correos
 * @property CatEstatus $catEstatus
 * @property ReciboPago[] $reciboPagos
 * @property RegistroApartamento[] $registroApartamentos
 * @property TelefonosHasPersona[] $telefonosHasPersonas
 * @property Telefonos[] $telefonos
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cat_estatus', 'primer_nombre_persona', 'segundo_nombre_persona', 'primer_apellido_persona', 'segundo_apellido_persona', 'ci_persona', 'dueno'], 'required'],
            [['id_cat_estatus', 'bactivo'], 'integer'],
            [['fecha_creacion', 'fecha_update'], 'safe'],
            [['primer_nombre_persona', 'segundo_nombre_persona', 'primer_apellido_persona', 'segundo_apellido_persona'], 'string', 'max' => 60],
            [['ci_persona'], 'string', 'max' => 20],
            [['dueno'], 'string', 'max' => 4],
            [['id_cat_estatus'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['id_cat_estatus' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cat_estatus' => 'Id Cat Estatus',
            'primer_nombre_persona' => 'Primer Nombre Persona',
            'segundo_nombre_persona' => 'Segundo Nombre Persona',
            'primer_apellido_persona' => 'Primer Apellido Persona',
            'segundo_apellido_persona' => 'Segundo Apellido Persona',
            'ci_persona' => 'Ci Persona',
            'dueno' => 'Dueno',
            'bactivo' => 'Bactivo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreosHasPersonas()
    {
        return $this->hasMany(CorreosHasPersona::className(), ['persona_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreos()
    {
        return $this->hasMany(Correos::className(), ['id' => 'correos_id'])->viaTable('correos_has_persona', ['persona_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatEstatus()
    {
        return $this->hasOne(CatEstatus::className(), ['id' => 'id_cat_estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboPagos()
    {
        return $this->hasMany(ReciboPago::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistroApartamentos()
    {
        return $this->hasMany(RegistroApartamento::className(), ['id_persona' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonosHasPersonas()
    {
        return $this->hasMany(TelefonosHasPersona::className(), ['persona_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['id' => 'telefonos_id'])->viaTable('telefonos_has_persona', ['persona_id' => 'id']);
    }
}
