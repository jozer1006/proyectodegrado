<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "correos".
 *
 * @property int $id
 * @property string $correo
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property string $fecha_update
 * @property int $cat_estatus_id
 *
 * @property CatEstatus $catEstatus
 * @property CorreosHasPersona[] $correosHasPersonas
 * @property Persona[] $personas
 */
class Correos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'correos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'correo', 'descripcion', 'cat_estatus_id'], 'required'],
            [['id', 'cat_estatus_id'], 'integer'],
            [['fecha_creacion', 'fecha_update'], 'safe'],
            [['correo'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 225],
            [['id', 'cat_estatus_id'], 'unique', 'targetAttribute' => ['id', 'cat_estatus_id']],
            [['cat_estatus_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['cat_estatus_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'correo' => 'Correo',
            'descripcion' => 'Descripcion',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
            'cat_estatus_id' => 'Cat Estatus ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatEstatus()
    {
        return $this->hasOne(CatEstatus::className(), ['id' => 'cat_estatus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreosHasPersonas()
    {
        return $this->hasMany(CorreosHasPersona::className(), ['correos_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['id' => 'persona_id'])->viaTable('correos_has_persona', ['correos_id' => 'id']);
    }
}
