<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cat_forma_pago".
 *
 * @property int $id
 * @property string $tipo_pago
 * @property int $bactivo
 * @property string $fecha_creacion
 * @property string $fecha_update
 *
 * @property ReciboPago[] $reciboPagos
 */
class CatFormaPago extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_forma_pago';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_pago'], 'required'],
            [['bactivo'], 'integer'],
            [['fecha_creacion', 'fecha_update'], 'safe'],
            [['tipo_pago'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_pago' => 'Tipo Pago',
            'bactivo' => 'Bactivo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboPagos()
    {
        return $this->hasMany(ReciboPago::className(), ['id_cat_forma_pago' => 'id']);
    }
}
