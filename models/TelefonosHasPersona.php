<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos_has_persona".
 *
 * @property int $telefonos_id
 * @property int $persona_id
 *
 * @property Telefonos $telefonos
 * @property Persona $persona
 */
class TelefonosHasPersona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos_has_persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefonos_id', 'persona_id'], 'required'],
            [['telefonos_id', 'persona_id'], 'integer'],
            [['telefonos_id', 'persona_id'], 'unique', 'targetAttribute' => ['telefonos_id', 'persona_id']],
            [['telefonos_id'], 'exist', 'skipOnError' => true, 'targetClass' => Telefonos::className(), 'targetAttribute' => ['telefonos_id' => 'id']],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'telefonos_id' => 'Telefonos ID',
            'persona_id' => 'Persona ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasOne(Telefonos::className(), ['id' => 'telefonos_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }
}
