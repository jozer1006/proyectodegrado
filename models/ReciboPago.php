<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recibo_pago".
 *
 * @property int $id
 * @property int $id_cat_estatus
 * @property int $id_cat_forma_pago
 * @property int $tabulador_general_id
 * @property int $id_persona
 * @property string $numero_recibo_comprobante
 * @property string $fecha_pago
 * @property double $monto_pagado
 * @property string $descripcion_pago
 * @property int $bactivo
 * @property string $fecha_creacion
 * @property string $fecha_update
 *
 * @property CatEstatus $catEstatus
 * @property CatFormaPago $catFormaPago
 * @property Persona $persona
 * @property TabuladorGeneral $tabuladorGeneral
 */
class ReciboPago extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recibo_pago';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cat_estatus', 'id_cat_forma_pago', 'tabulador_general_id', 'id_persona', 'numero_recibo_comprobante', 'monto_pagado', 'descripcion_pago'], 'required'],
            [['id_cat_estatus', 'id_cat_forma_pago', 'tabulador_general_id', 'id_persona', 'bactivo'], 'integer'],
            [['fecha_pago', 'fecha_creacion', 'fecha_update'], 'safe'],
            [['monto_pagado'], 'number'],
            [['numero_recibo_comprobante'], 'string', 'max' => 45],
            [['descripcion_pago'], 'string', 'max' => 150],
            [['id_cat_estatus'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['id_cat_estatus' => 'id']],
            [['id_cat_forma_pago'], 'exist', 'skipOnError' => true, 'targetClass' => CatFormaPago::className(), 'targetAttribute' => ['id_cat_forma_pago' => 'id']],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['id_persona' => 'id']],
            [['tabulador_general_id'], 'exist', 'skipOnError' => true, 'targetClass' => TabuladorGeneral::className(), 'targetAttribute' => ['tabulador_general_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cat_estatus' => 'Id Cat Estatus',
            'id_cat_forma_pago' => 'Id Cat Forma Pago',
            'tabulador_general_id' => 'Tabulador General ID',
            'id_persona' => 'Id Persona',
            'numero_recibo_comprobante' => 'Numero Recibo Comprobante',
            'fecha_pago' => 'Fecha Pago',
            'monto_pagado' => 'Monto Pagado',
            'descripcion_pago' => 'Descripcion Pago',
            'bactivo' => 'Bactivo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatEstatus()
    {
        return $this->hasOne(CatEstatus::className(), ['id' => 'id_cat_estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatFormaPago()
    {
        return $this->hasOne(CatFormaPago::className(), ['id' => 'id_cat_forma_pago']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabuladorGeneral()
    {
        return $this->hasOne(TabuladorGeneral::className(), ['id' => 'tabulador_general_id']);
    }
}
