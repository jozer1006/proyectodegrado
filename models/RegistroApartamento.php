<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registro_apartamento".
 *
 * @property int $id
 * @property int $id_cat_estatus
 * @property int $id_persona
 * @property string $numero_apto
 * @property string $num_piso
 * @property int $bactivo
 * @property string $fecha_creacion
 * @property string $fecha_update
 *
 * @property CatEstatus $catEstatus
 * @property Persona $persona
 */
class RegistroApartamento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registro_apartamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cat_estatus', 'id_persona', 'numero_apto', 'num_piso'], 'required'],
            [['id_cat_estatus', 'id_persona', 'bactivo'], 'integer'],
            [['fecha_creacion', 'fecha_update'], 'safe'],
            [['numero_apto'], 'string', 'max' => 10],
            [['num_piso'], 'string', 'max' => 5],
            [['id_cat_estatus'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['id_cat_estatus' => 'id']],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['id_persona' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cat_estatus' => 'Id Cat Estatus',
            'id_persona' => 'Id Persona',
            'numero_apto' => 'Numero Apto',
            'num_piso' => 'Num Piso',
            'bactivo' => 'Bactivo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatEstatus()
    {
        return $this->hasOne(CatEstatus::className(), ['id' => 'id_cat_estatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'id_persona']);
    }
}
