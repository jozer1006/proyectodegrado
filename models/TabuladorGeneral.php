<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tabulador_general".
 *
 * @property int $id
 * @property double $monto_a_pagar
 * @property string $fecha_tope_a_pagar
 * @property string $mes_a_pagar
 * @property string $observaciones
 * @property string $fecha_creacion
 * @property string $fecha_update
 * @property int $cat_estatus_id
 *
 * @property ReciboPago[] $reciboPagos
 * @property CatEstatus $catEstatus
 */
class TabuladorGeneral extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tabulador_general';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'monto_a_pagar', 'fecha_tope_a_pagar', 'mes_a_pagar', 'cat_estatus_id'], 'required'],
            [['id', 'cat_estatus_id'], 'integer'],
            [['monto_a_pagar'], 'number'],
            [['fecha_tope_a_pagar', 'mes_a_pagar', 'fecha_creacion', 'fecha_update'], 'safe'],
            [['observaciones'], 'string', 'max' => 255],
            [['mes_a_pagar'], 'unique'],
            [['id'], 'unique'],
            [['cat_estatus_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['cat_estatus_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'monto_a_pagar' => 'Monto A Pagar',
            'fecha_tope_a_pagar' => 'Fecha Tope A Pagar',
            'mes_a_pagar' => 'Mes A Pagar',
            'observaciones' => 'Observaciones',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
            'cat_estatus_id' => 'Cat Estatus ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboPagos()
    {
        return $this->hasMany(ReciboPago::className(), ['tabulador_general_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatEstatus()
    {
        return $this->hasOne(CatEstatus::className(), ['id' => 'cat_estatus_id']);
    }
}
