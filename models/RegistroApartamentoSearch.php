<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RegistroApartamento;

/**
 * RegistroApartamentoSearch represents the model behind the search form of `\app\models\RegistroApartamento`.
 */
class RegistroApartamentoSearch extends RegistroApartamento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_cat_estatus', 'id_persona', 'bactivo'], 'integer'],
            [['numero_apto', 'num_piso', 'fecha_creacion', 'fecha_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegistroApartamento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cat_estatus' => $this->id_cat_estatus,
            'id_persona' => $this->id_persona,
            'bactivo' => $this->bactivo,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_update' => $this->fecha_update,
        ]);

        $query->andFilterWhere(['like', 'numero_apto', $this->numero_apto])
            ->andFilterWhere(['like', 'num_piso', $this->num_piso]);

        return $dataProvider;
    }
}
