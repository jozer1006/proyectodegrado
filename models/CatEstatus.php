<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cat_estatus".
 *
 * @property int $id
 * @property string $nombre_estatus
 * @property string $descripcion_estatus
 * @property int $bactivo
 * @property string $fecha_creacion
 * @property string $fecha_update
 *
 * @property Correos[] $correos
 * @property Persona[] $personas
 * @property ReciboPago[] $reciboPagos
 * @property RegistroApartamento[] $registroApartamentos
 * @property TabuladorGeneral[] $tabuladorGenerals
 * @property Telefonos[] $telefonos
 */
class CatEstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_estatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_estatus', 'descripcion_estatus'], 'required'],
            [['bactivo'], 'integer'],
            [['fecha_creacion', 'fecha_update'], 'safe'],
            [['nombre_estatus'], 'string', 'max' => 20],
            [['descripcion_estatus'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_estatus' => 'Nombre Estatus',
            'descripcion_estatus' => 'Descripcion Estatus',
            'bactivo' => 'Bactivo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_update' => 'Fecha Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorreos()
    {
        return $this->hasMany(Correos::className(), ['cat_estatus_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['id_cat_estatus' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboPagos()
    {
        return $this->hasMany(ReciboPago::className(), ['id_cat_estatus' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistroApartamentos()
    {
        return $this->hasMany(RegistroApartamento::className(), ['id_cat_estatus' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabuladorGenerals()
    {
        return $this->hasMany(TabuladorGeneral::className(), ['cat_estatus_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['cat_estatus_id' => 'id']);
    }
}
