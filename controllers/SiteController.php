<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
/*use app\models\ContactForm;
use app\models\ContactoForm;
use app\models\ContactosForm;
use app\models\PropietariosForm;
use app\models\RecibosForm;*/

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   $this->layout = null;
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
/********************************************************************************************/
                               /*modificacion*/
    /**
     * Displays contacto page.
     *
     * @return Response|string
     */
    public function actionContacto()
    {
        $model = new ContactoForm();
        if ($model->load(Yii::$app->request->post()) && $model->contacto(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactoFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contacto', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contactos page.
     *
     * @return Response|string
     */
    public function actionContactos()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contactos(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactosFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contactos', [
            'model' => $model,
        ]);
    }

/********************************************************************************************/

    /**
     * Displays Propietarios page.
     *
     * @return Response|string
     */
    public function actionPropietarios()
    {
        $model = new PropietariosForm();
        if ($model->load(Yii::$app->request->post()) && $model->propietarios(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('propietariosFormSubmitted');

            return $this->refresh();
        }
        return $this->render('propietarios', [
            'model' => $model,
        ]);
    }


/********************************************************************************************/

    /**
     * Displays Propietarios page.
     *
     * @return Response|string
     */
    public function actionRecibos()
    {
        $model = new RecibosForm();
        if ($model->load(Yii::$app->request->post()) && $model->recibos(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('recibosFormSubmitted');

            return $this->refresh();
        }
        return $this->render('recibos', [
            'model' => $model,
        ]);
    }


/********************************************************************************************/

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
